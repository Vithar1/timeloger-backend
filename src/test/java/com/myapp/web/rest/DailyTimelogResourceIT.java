package com.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.myapp.IntegrationTest;
import com.myapp.domain.DailyTimelog;
import com.myapp.repository.DailyTimelogRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DailyTimelogResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class DailyTimelogResourceIT {

    private static final Instant DEFAULT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/daily-timelogs";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DailyTimelogRepository dailyTimelogRepository;

    @Mock
    private DailyTimelogRepository dailyTimelogRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDailyTimelogMockMvc;

    private DailyTimelog dailyTimelog;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DailyTimelog createEntity(EntityManager em) {
        DailyTimelog dailyTimelog = new DailyTimelog().time(DEFAULT_TIME).description(DEFAULT_DESCRIPTION).title(DEFAULT_TITLE);
        return dailyTimelog;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DailyTimelog createUpdatedEntity(EntityManager em) {
        DailyTimelog dailyTimelog = new DailyTimelog().time(UPDATED_TIME).description(UPDATED_DESCRIPTION).title(UPDATED_TITLE);
        return dailyTimelog;
    }

    @BeforeEach
    public void initTest() {
        dailyTimelog = createEntity(em);
    }

    @Test
    @Transactional
    void createDailyTimelog() throws Exception {
        int databaseSizeBeforeCreate = dailyTimelogRepository.findAll().size();
        // Create the DailyTimelog
        restDailyTimelogMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dailyTimelog)))
            .andExpect(status().isCreated());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeCreate + 1);
        DailyTimelog testDailyTimelog = dailyTimelogList.get(dailyTimelogList.size() - 1);
        assertThat(testDailyTimelog.getTime()).isEqualTo(DEFAULT_TIME);
        assertThat(testDailyTimelog.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDailyTimelog.getTitle()).isEqualTo(DEFAULT_TITLE);
    }

    @Test
    @Transactional
    void createDailyTimelogWithExistingId() throws Exception {
        // Create the DailyTimelog with an existing ID
        dailyTimelog.setId(1L);

        int databaseSizeBeforeCreate = dailyTimelogRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDailyTimelogMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dailyTimelog)))
            .andExpect(status().isBadRequest());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllDailyTimelogs() throws Exception {
        // Initialize the database
        dailyTimelogRepository.saveAndFlush(dailyTimelog);

        // Get all the dailyTimelogList
        restDailyTimelogMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dailyTimelog.getId().intValue())))
            .andExpect(jsonPath("$.[*].time").value(hasItem(DEFAULT_TIME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDailyTimelogsWithEagerRelationshipsIsEnabled() throws Exception {
        when(dailyTimelogRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDailyTimelogMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(dailyTimelogRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllDailyTimelogsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(dailyTimelogRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restDailyTimelogMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(dailyTimelogRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getDailyTimelog() throws Exception {
        // Initialize the database
        dailyTimelogRepository.saveAndFlush(dailyTimelog);

        // Get the dailyTimelog
        restDailyTimelogMockMvc
            .perform(get(ENTITY_API_URL_ID, dailyTimelog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dailyTimelog.getId().intValue()))
            .andExpect(jsonPath("$.time").value(DEFAULT_TIME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE));
    }

    @Test
    @Transactional
    void getNonExistingDailyTimelog() throws Exception {
        // Get the dailyTimelog
        restDailyTimelogMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDailyTimelog() throws Exception {
        // Initialize the database
        dailyTimelogRepository.saveAndFlush(dailyTimelog);

        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();

        // Update the dailyTimelog
        DailyTimelog updatedDailyTimelog = dailyTimelogRepository.findById(dailyTimelog.getId()).get();
        // Disconnect from session so that the updates on updatedDailyTimelog are not directly saved in db
        em.detach(updatedDailyTimelog);
        updatedDailyTimelog.time(UPDATED_TIME).description(UPDATED_DESCRIPTION).title(UPDATED_TITLE);

        restDailyTimelogMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedDailyTimelog.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedDailyTimelog))
            )
            .andExpect(status().isOk());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
        DailyTimelog testDailyTimelog = dailyTimelogList.get(dailyTimelogList.size() - 1);
        assertThat(testDailyTimelog.getTime()).isEqualTo(UPDATED_TIME);
        assertThat(testDailyTimelog.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDailyTimelog.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    @Transactional
    void putNonExistingDailyTimelog() throws Exception {
        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();
        dailyTimelog.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDailyTimelogMockMvc
            .perform(
                put(ENTITY_API_URL_ID, dailyTimelog.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dailyTimelog))
            )
            .andExpect(status().isBadRequest());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDailyTimelog() throws Exception {
        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();
        dailyTimelog.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDailyTimelogMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(dailyTimelog))
            )
            .andExpect(status().isBadRequest());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDailyTimelog() throws Exception {
        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();
        dailyTimelog.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDailyTimelogMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dailyTimelog)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDailyTimelogWithPatch() throws Exception {
        // Initialize the database
        dailyTimelogRepository.saveAndFlush(dailyTimelog);

        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();

        // Update the dailyTimelog using partial update
        DailyTimelog partialUpdatedDailyTimelog = new DailyTimelog();
        partialUpdatedDailyTimelog.setId(dailyTimelog.getId());

        partialUpdatedDailyTimelog.description(UPDATED_DESCRIPTION).title(UPDATED_TITLE);

        restDailyTimelogMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDailyTimelog.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDailyTimelog))
            )
            .andExpect(status().isOk());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
        DailyTimelog testDailyTimelog = dailyTimelogList.get(dailyTimelogList.size() - 1);
        assertThat(testDailyTimelog.getTime()).isEqualTo(DEFAULT_TIME);
        assertThat(testDailyTimelog.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDailyTimelog.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    @Transactional
    void fullUpdateDailyTimelogWithPatch() throws Exception {
        // Initialize the database
        dailyTimelogRepository.saveAndFlush(dailyTimelog);

        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();

        // Update the dailyTimelog using partial update
        DailyTimelog partialUpdatedDailyTimelog = new DailyTimelog();
        partialUpdatedDailyTimelog.setId(dailyTimelog.getId());

        partialUpdatedDailyTimelog.time(UPDATED_TIME).description(UPDATED_DESCRIPTION).title(UPDATED_TITLE);

        restDailyTimelogMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDailyTimelog.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDailyTimelog))
            )
            .andExpect(status().isOk());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
        DailyTimelog testDailyTimelog = dailyTimelogList.get(dailyTimelogList.size() - 1);
        assertThat(testDailyTimelog.getTime()).isEqualTo(UPDATED_TIME);
        assertThat(testDailyTimelog.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDailyTimelog.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    @Transactional
    void patchNonExistingDailyTimelog() throws Exception {
        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();
        dailyTimelog.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDailyTimelogMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, dailyTimelog.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dailyTimelog))
            )
            .andExpect(status().isBadRequest());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDailyTimelog() throws Exception {
        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();
        dailyTimelog.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDailyTimelogMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(dailyTimelog))
            )
            .andExpect(status().isBadRequest());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDailyTimelog() throws Exception {
        int databaseSizeBeforeUpdate = dailyTimelogRepository.findAll().size();
        dailyTimelog.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDailyTimelogMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dailyTimelog))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DailyTimelog in the database
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDailyTimelog() throws Exception {
        // Initialize the database
        dailyTimelogRepository.saveAndFlush(dailyTimelog);

        int databaseSizeBeforeDelete = dailyTimelogRepository.findAll().size();

        // Delete the dailyTimelog
        restDailyTimelogMockMvc
            .perform(delete(ENTITY_API_URL_ID, dailyTimelog.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DailyTimelog> dailyTimelogList = dailyTimelogRepository.findAll();
        assertThat(dailyTimelogList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
