package com.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DailyTimelogTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DailyTimelog.class);
        DailyTimelog dailyTimelog1 = new DailyTimelog();
        dailyTimelog1.setId(1L);
        DailyTimelog dailyTimelog2 = new DailyTimelog();
        dailyTimelog2.setId(dailyTimelog1.getId());
        assertThat(dailyTimelog1).isEqualTo(dailyTimelog2);
        dailyTimelog2.setId(2L);
        assertThat(dailyTimelog1).isNotEqualTo(dailyTimelog2);
        dailyTimelog1.setId(null);
        assertThat(dailyTimelog1).isNotEqualTo(dailyTimelog2);
    }
}
