import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './task.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const TaskDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const taskEntity = useAppSelector(state => state.task.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="taskDetailsHeading">
          <Translate contentKey="timelogerBackendApp.task.detail.title">Task</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="timelogerBackendApp.task.id">Id</Translate>
            </span>
          </dt>
          <dd>{taskEntity.id}</dd>
          <dt>
            <span id="spendTime">
              <Translate contentKey="timelogerBackendApp.task.spendTime">Spend Time</Translate>
            </span>
          </dt>
          <dd>{taskEntity.spendTime ? <TextFormat value={taskEntity.spendTime} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="state">
              <Translate contentKey="timelogerBackendApp.task.state">State</Translate>
            </span>
          </dt>
          <dd>{taskEntity.state}</dd>
          <dt>
            <span id="title">
              <Translate contentKey="timelogerBackendApp.task.title">Title</Translate>
            </span>
          </dt>
          <dd>{taskEntity.title}</dd>
          <dt>
            <span id="description">
              <Translate contentKey="timelogerBackendApp.task.description">Description</Translate>
            </span>
          </dt>
          <dd>{taskEntity.description}</dd>
          <dt>
            <Translate contentKey="timelogerBackendApp.task.project">Project</Translate>
          </dt>
          <dd>{taskEntity.project ? taskEntity.project.id : ''}</dd>
          <dt>
            <Translate contentKey="timelogerBackendApp.task.type">Type</Translate>
          </dt>
          <dd>{taskEntity.type ? taskEntity.type.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/task" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/task/${taskEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default TaskDetail;
