import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntities } from './daily-timelog.reducer';
import { IDailyTimelog } from 'app/shared/model/daily-timelog.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const DailyTimelog = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const dailyTimelogList = useAppSelector(state => state.dailyTimelog.entities);
  const loading = useAppSelector(state => state.dailyTimelog.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  return (
    <div>
      <h2 id="daily-timelog-heading" data-cy="DailyTimelogHeading">
        <Translate contentKey="timelogerBackendApp.dailyTimelog.home.title">Daily Timelogs</Translate>
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="timelogerBackendApp.dailyTimelog.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="timelogerBackendApp.dailyTimelog.home.createLabel">Create new Daily Timelog</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {dailyTimelogList && dailyTimelogList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="timelogerBackendApp.dailyTimelog.id">Id</Translate>
                </th>
                <th>
                  <Translate contentKey="timelogerBackendApp.dailyTimelog.time">Time</Translate>
                </th>
                <th>
                  <Translate contentKey="timelogerBackendApp.dailyTimelog.description">Description</Translate>
                </th>
                <th>
                  <Translate contentKey="timelogerBackendApp.dailyTimelog.title">Title</Translate>
                </th>
                <th>
                  <Translate contentKey="timelogerBackendApp.dailyTimelog.task">Task</Translate>
                </th>
                <th>
                  <Translate contentKey="timelogerBackendApp.dailyTimelog.users">Users</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {dailyTimelogList.map((dailyTimelog, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${dailyTimelog.id}`} color="link" size="sm">
                      {dailyTimelog.id}
                    </Button>
                  </td>
                  <td>{dailyTimelog.time ? <TextFormat type="date" value={dailyTimelog.time} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>{dailyTimelog.description}</td>
                  <td>{dailyTimelog.title}</td>
                  <td>{dailyTimelog.task ? <Link to={`task/${dailyTimelog.task.id}`}>{dailyTimelog.task.id}</Link> : ''}</td>
                  <td>
                    {dailyTimelog.users
                      ? dailyTimelog.users.map((val, j) => (
                          <span key={j}>
                            {val.id}
                            {j === dailyTimelog.users.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${dailyTimelog.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${dailyTimelog.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${dailyTimelog.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="timelogerBackendApp.dailyTimelog.home.notFound">No Daily Timelogs found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default DailyTimelog;
