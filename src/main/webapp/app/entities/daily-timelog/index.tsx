import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import DailyTimelog from './daily-timelog';
import DailyTimelogDetail from './daily-timelog-detail';
import DailyTimelogUpdate from './daily-timelog-update';
import DailyTimelogDeleteDialog from './daily-timelog-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DailyTimelogUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={DailyTimelogUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={DailyTimelogDetail} />
      <ErrorBoundaryRoute path={match.url} component={DailyTimelog} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={DailyTimelogDeleteDialog} />
  </>
);

export default Routes;
