import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './daily-timelog.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const DailyTimelogDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const dailyTimelogEntity = useAppSelector(state => state.dailyTimelog.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="dailyTimelogDetailsHeading">
          <Translate contentKey="timelogerBackendApp.dailyTimelog.detail.title">DailyTimelog</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="timelogerBackendApp.dailyTimelog.id">Id</Translate>
            </span>
          </dt>
          <dd>{dailyTimelogEntity.id}</dd>
          <dt>
            <span id="time">
              <Translate contentKey="timelogerBackendApp.dailyTimelog.time">Time</Translate>
            </span>
          </dt>
          <dd>{dailyTimelogEntity.time ? <TextFormat value={dailyTimelogEntity.time} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="description">
              <Translate contentKey="timelogerBackendApp.dailyTimelog.description">Description</Translate>
            </span>
          </dt>
          <dd>{dailyTimelogEntity.description}</dd>
          <dt>
            <span id="title">
              <Translate contentKey="timelogerBackendApp.dailyTimelog.title">Title</Translate>
            </span>
          </dt>
          <dd>{dailyTimelogEntity.title}</dd>
          <dt>
            <Translate contentKey="timelogerBackendApp.dailyTimelog.task">Task</Translate>
          </dt>
          <dd>{dailyTimelogEntity.task ? dailyTimelogEntity.task.id : ''}</dd>
          <dt>
            <Translate contentKey="timelogerBackendApp.dailyTimelog.users">Users</Translate>
          </dt>
          <dd>
            {dailyTimelogEntity.users
              ? dailyTimelogEntity.users.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {dailyTimelogEntity.users && i === dailyTimelogEntity.users.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
        </dl>
        <Button tag={Link} to="/daily-timelog" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/daily-timelog/${dailyTimelogEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default DailyTimelogDetail;
