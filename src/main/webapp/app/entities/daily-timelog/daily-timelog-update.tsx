import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ITask } from 'app/shared/model/task.model';
import { getEntities as getTasks } from 'app/entities/task/task.reducer';
import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { getEntity, updateEntity, createEntity, reset } from './daily-timelog.reducer';
import { IDailyTimelog } from 'app/shared/model/daily-timelog.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const DailyTimelogUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const tasks = useAppSelector(state => state.task.entities);
  const users = useAppSelector(state => state.userManagement.users);
  const dailyTimelogEntity = useAppSelector(state => state.dailyTimelog.entity);
  const loading = useAppSelector(state => state.dailyTimelog.loading);
  const updating = useAppSelector(state => state.dailyTimelog.updating);
  const updateSuccess = useAppSelector(state => state.dailyTimelog.updateSuccess);
  const handleClose = () => {
    props.history.push('/daily-timelog');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getTasks({}));
    dispatch(getUsers({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    values.time = convertDateTimeToServer(values.time);

    const entity = {
      ...dailyTimelogEntity,
      ...values,
      users: mapIdList(values.users),
      task: tasks.find(it => it.id.toString() === values.task.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          time: displayDefaultDateTime(),
        }
      : {
          ...dailyTimelogEntity,
          time: convertDateTimeFromServer(dailyTimelogEntity.time),
          task: dailyTimelogEntity?.task?.id,
          users: dailyTimelogEntity?.users?.map(e => e.id.toString()),
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="timelogerBackendApp.dailyTimelog.home.createOrEditLabel" data-cy="DailyTimelogCreateUpdateHeading">
            <Translate contentKey="timelogerBackendApp.dailyTimelog.home.createOrEditLabel">Create or edit a DailyTimelog</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="daily-timelog-id"
                  label={translate('timelogerBackendApp.dailyTimelog.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('timelogerBackendApp.dailyTimelog.time')}
                id="daily-timelog-time"
                name="time"
                data-cy="time"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('timelogerBackendApp.dailyTimelog.description')}
                id="daily-timelog-description"
                name="description"
                data-cy="description"
                type="text"
              />
              <ValidatedField
                label={translate('timelogerBackendApp.dailyTimelog.title')}
                id="daily-timelog-title"
                name="title"
                data-cy="title"
                type="text"
              />
              <ValidatedField
                id="daily-timelog-task"
                name="task"
                data-cy="task"
                label={translate('timelogerBackendApp.dailyTimelog.task')}
                type="select"
              >
                <option value="" key="0" />
                {tasks
                  ? tasks.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                label={translate('timelogerBackendApp.dailyTimelog.users')}
                id="daily-timelog-users"
                data-cy="users"
                type="select"
                multiple
                name="users"
              >
                <option value="" key="0" />
                {users
                  ? users.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/daily-timelog" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default DailyTimelogUpdate;
