import { ICompany } from 'app/shared/model/company.model';
import { IUser } from 'app/shared/model/user.model';

export interface IProject {
  id?: number;
  name?: string | null;
  company?: ICompany | null;
  users?: IUser[] | null;
}

export const defaultValue: Readonly<IProject> = {};
