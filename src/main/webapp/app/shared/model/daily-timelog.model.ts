import dayjs from 'dayjs';
import { ITask } from 'app/shared/model/task.model';
import { IUser } from 'app/shared/model/user.model';

export interface IDailyTimelog {
  id?: number;
  time?: string | null;
  description?: string | null;
  title?: string | null;
  task?: ITask | null;
  users?: IUser[] | null;
}

export const defaultValue: Readonly<IDailyTimelog> = {};
