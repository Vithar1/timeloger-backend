import dayjs from 'dayjs';
import { IProject } from 'app/shared/model/project.model';
import { ITaskType } from 'app/shared/model/task-type.model';

export interface ITask {
  id?: number;
  spendTime?: string | null;
  state?: number | null;
  title?: string | null;
  description?: string | null;
  project?: IProject | null;
  type?: ITaskType | null;
}

export const defaultValue: Readonly<ITask> = {};
