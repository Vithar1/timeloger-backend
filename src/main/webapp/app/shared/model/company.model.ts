import { IUser } from 'app/shared/model/user.model';

export interface ICompany {
  id?: number;
  name?: string | null;
  creator?: IUser | null;
  users?: IUser[] | null;
}

export const defaultValue: Readonly<ICompany> = {};
