export interface ITaskType {
  id?: number;
  title?: string | null;
}

export const defaultValue: Readonly<ITaskType> = {};
