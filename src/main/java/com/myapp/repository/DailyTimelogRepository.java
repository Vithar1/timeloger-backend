package com.myapp.repository;

import com.myapp.domain.DailyTimelog;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DailyTimelog entity.
 */
@Repository
public interface DailyTimelogRepository extends JpaRepository<DailyTimelog, Long> {
    @Query(
        value = "select distinct dailyTimelog from DailyTimelog dailyTimelog left join fetch dailyTimelog.users",
        countQuery = "select count(distinct dailyTimelog) from DailyTimelog dailyTimelog"
    )
    Page<DailyTimelog> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct dailyTimelog from DailyTimelog dailyTimelog left join fetch dailyTimelog.users")
    List<DailyTimelog> findAllWithEagerRelationships();

    @Query("select dailyTimelog from DailyTimelog dailyTimelog left join fetch dailyTimelog.users where dailyTimelog.id =:id")
    Optional<DailyTimelog> findOneWithEagerRelationships(@Param("id") Long id);
}
