package com.myapp.repository;

import com.myapp.domain.Company;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Company entity.
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    @Query("select company from Company company where company.creator.login = ?#{principal.username}")
    List<Company> findByCreatorIsCurrentUser();

    @Query(
        value = "select distinct company from Company company left join fetch company.users",
        countQuery = "select count(distinct company) from Company company"
    )
    Page<Company> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct company from Company company left join fetch company.users")
    List<Company> findAllWithEagerRelationships();

    @Query("select company from Company company left join fetch company.users where company.id =:id")
    Optional<Company> findOneWithEagerRelationships(@Param("id") Long id);
}
