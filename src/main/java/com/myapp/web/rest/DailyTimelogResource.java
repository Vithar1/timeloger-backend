package com.myapp.web.rest;

import com.myapp.domain.DailyTimelog;
import com.myapp.repository.DailyTimelogRepository;
import com.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.myapp.domain.DailyTimelog}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class DailyTimelogResource {

    private final Logger log = LoggerFactory.getLogger(DailyTimelogResource.class);

    private static final String ENTITY_NAME = "dailyTimelog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DailyTimelogRepository dailyTimelogRepository;

    public DailyTimelogResource(DailyTimelogRepository dailyTimelogRepository) {
        this.dailyTimelogRepository = dailyTimelogRepository;
    }

    /**
     * {@code POST  /daily-timelogs} : Create a new dailyTimelog.
     *
     * @param dailyTimelog the dailyTimelog to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dailyTimelog, or with status {@code 400 (Bad Request)} if the dailyTimelog has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/daily-timelogs")
    public ResponseEntity<DailyTimelog> createDailyTimelog(@RequestBody DailyTimelog dailyTimelog) throws URISyntaxException {
        log.debug("REST request to save DailyTimelog : {}", dailyTimelog);
        if (dailyTimelog.getId() != null) {
            throw new BadRequestAlertException("A new dailyTimelog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DailyTimelog result = dailyTimelogRepository.save(dailyTimelog);
        return ResponseEntity
            .created(new URI("/api/daily-timelogs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /daily-timelogs/:id} : Updates an existing dailyTimelog.
     *
     * @param id the id of the dailyTimelog to save.
     * @param dailyTimelog the dailyTimelog to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dailyTimelog,
     * or with status {@code 400 (Bad Request)} if the dailyTimelog is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dailyTimelog couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/daily-timelogs/{id}")
    public ResponseEntity<DailyTimelog> updateDailyTimelog(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DailyTimelog dailyTimelog
    ) throws URISyntaxException {
        log.debug("REST request to update DailyTimelog : {}, {}", id, dailyTimelog);
        if (dailyTimelog.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dailyTimelog.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dailyTimelogRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DailyTimelog result = dailyTimelogRepository.save(dailyTimelog);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dailyTimelog.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /daily-timelogs/:id} : Partial updates given fields of an existing dailyTimelog, field will ignore if it is null
     *
     * @param id the id of the dailyTimelog to save.
     * @param dailyTimelog the dailyTimelog to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dailyTimelog,
     * or with status {@code 400 (Bad Request)} if the dailyTimelog is not valid,
     * or with status {@code 404 (Not Found)} if the dailyTimelog is not found,
     * or with status {@code 500 (Internal Server Error)} if the dailyTimelog couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/daily-timelogs/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DailyTimelog> partialUpdateDailyTimelog(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DailyTimelog dailyTimelog
    ) throws URISyntaxException {
        log.debug("REST request to partial update DailyTimelog partially : {}, {}", id, dailyTimelog);
        if (dailyTimelog.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dailyTimelog.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dailyTimelogRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DailyTimelog> result = dailyTimelogRepository
            .findById(dailyTimelog.getId())
            .map(existingDailyTimelog -> {
                if (dailyTimelog.getTime() != null) {
                    existingDailyTimelog.setTime(dailyTimelog.getTime());
                }
                if (dailyTimelog.getDescription() != null) {
                    existingDailyTimelog.setDescription(dailyTimelog.getDescription());
                }
                if (dailyTimelog.getTitle() != null) {
                    existingDailyTimelog.setTitle(dailyTimelog.getTitle());
                }

                return existingDailyTimelog;
            })
            .map(dailyTimelogRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, dailyTimelog.getId().toString())
        );
    }

    /**
     * {@code GET  /daily-timelogs} : get all the dailyTimelogs.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dailyTimelogs in body.
     */
    @GetMapping("/daily-timelogs")
    public List<DailyTimelog> getAllDailyTimelogs(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all DailyTimelogs");
        return dailyTimelogRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /daily-timelogs/:id} : get the "id" dailyTimelog.
     *
     * @param id the id of the dailyTimelog to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dailyTimelog, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/daily-timelogs/{id}")
    public ResponseEntity<DailyTimelog> getDailyTimelog(@PathVariable Long id) {
        log.debug("REST request to get DailyTimelog : {}", id);
        Optional<DailyTimelog> dailyTimelog = dailyTimelogRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(dailyTimelog);
    }

    /**
     * {@code DELETE  /daily-timelogs/:id} : delete the "id" dailyTimelog.
     *
     * @param id the id of the dailyTimelog to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/daily-timelogs/{id}")
    public ResponseEntity<Void> deleteDailyTimelog(@PathVariable Long id) {
        log.debug("REST request to delete DailyTimelog : {}", id);
        dailyTimelogRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
