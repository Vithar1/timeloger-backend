package com.myapp.web.rest;

import com.myapp.domain.TaskType;
import com.myapp.repository.TaskTypeRepository;
import com.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.myapp.domain.TaskType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TaskTypeResource {

    private final Logger log = LoggerFactory.getLogger(TaskTypeResource.class);

    private static final String ENTITY_NAME = "taskType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TaskTypeRepository taskTypeRepository;

    public TaskTypeResource(TaskTypeRepository taskTypeRepository) {
        this.taskTypeRepository = taskTypeRepository;
    }

    /**
     * {@code POST  /task-types} : Create a new taskType.
     *
     * @param taskType the taskType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new taskType, or with status {@code 400 (Bad Request)} if the taskType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/task-types")
    public ResponseEntity<TaskType> createTaskType(@RequestBody TaskType taskType) throws URISyntaxException {
        log.debug("REST request to save TaskType : {}", taskType);
        if (taskType.getId() != null) {
            throw new BadRequestAlertException("A new taskType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TaskType result = taskTypeRepository.save(taskType);
        return ResponseEntity
            .created(new URI("/api/task-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /task-types/:id} : Updates an existing taskType.
     *
     * @param id the id of the taskType to save.
     * @param taskType the taskType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated taskType,
     * or with status {@code 400 (Bad Request)} if the taskType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the taskType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/task-types/{id}")
    public ResponseEntity<TaskType> updateTaskType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TaskType taskType
    ) throws URISyntaxException {
        log.debug("REST request to update TaskType : {}, {}", id, taskType);
        if (taskType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, taskType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!taskTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TaskType result = taskTypeRepository.save(taskType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, taskType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /task-types/:id} : Partial updates given fields of an existing taskType, field will ignore if it is null
     *
     * @param id the id of the taskType to save.
     * @param taskType the taskType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated taskType,
     * or with status {@code 400 (Bad Request)} if the taskType is not valid,
     * or with status {@code 404 (Not Found)} if the taskType is not found,
     * or with status {@code 500 (Internal Server Error)} if the taskType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/task-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TaskType> partialUpdateTaskType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TaskType taskType
    ) throws URISyntaxException {
        log.debug("REST request to partial update TaskType partially : {}, {}", id, taskType);
        if (taskType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, taskType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!taskTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TaskType> result = taskTypeRepository
            .findById(taskType.getId())
            .map(existingTaskType -> {
                if (taskType.getTitle() != null) {
                    existingTaskType.setTitle(taskType.getTitle());
                }

                return existingTaskType;
            })
            .map(taskTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, taskType.getId().toString())
        );
    }

    /**
     * {@code GET  /task-types} : get all the taskTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of taskTypes in body.
     */
    @GetMapping("/task-types")
    public List<TaskType> getAllTaskTypes() {
        log.debug("REST request to get all TaskTypes");
        return taskTypeRepository.findAll();
    }

    /**
     * {@code GET  /task-types/:id} : get the "id" taskType.
     *
     * @param id the id of the taskType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the taskType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/task-types/{id}")
    public ResponseEntity<TaskType> getTaskType(@PathVariable Long id) {
        log.debug("REST request to get TaskType : {}", id);
        Optional<TaskType> taskType = taskTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(taskType);
    }

    /**
     * {@code DELETE  /task-types/:id} : delete the "id" taskType.
     *
     * @param id the id of the taskType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/task-types/{id}")
    public ResponseEntity<Void> deleteTaskType(@PathVariable Long id) {
        log.debug("REST request to delete TaskType : {}", id);
        taskTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
